CREATE TABLE IF NOT EXISTS users(
    id UUID PRIMARY KEY,
    user_data JSONB,
    calendars_refreshing BOOLEAN NOT NULL DEFAULT FALSE,
    calendars_updated_at TIMESTAMP WITHOUT TIME ZONE
);
CREATE TABLE IF NOT EXISTS google_accounts(
    id CHARACTER VARYING(255) PRIMARY KEY,
    user_id UUID REFERENCES users(id) NOT NULL,
    display_name CHARACTER VARYING(255) NOT NULL,
    access_token CHARACTER VARYING(2048),
    refresh_token CHARACTER VARYING(2048),
    expires_at TIMESTAMP WITHOUT TIME ZONE,
    created_at TIMESTAMP WITHOUT TIME ZONE NOT NULL,
    updated_at TIMESTAMP WITHOUT TIME ZONE NOT NULL
);
CREATE TABLE IF NOT EXISTS calendars(
    id UUID PRIMARY KEY,
    user_id UUID REFERENCES users(id) NOT NULL,
    display_name CHARACTER VARYING(255) NOT NULL,
    timezone CHARACTER VARYING(128) NOT NULL,
    is_tracked BOOLEAN NOT NULL
);
CREATE TABLE IF NOT EXISTS google_calendars(
    google_account_id CHARACTER VARYING(255) REFERENCES google_accounts(id) NOT NULL,
    calendar_id UUID REFERENCES calendars(id) NOT NULL,
    google_calendar_id CHARACTER VARYING(255) NOT NULL UNIQUE
);
CREATE TABLE IF NOT EXISTS google_calendar_spans(
    calendar_id UUID REFERENCES calendars(id),
    timespan TSRANGE NOT NULL,
    updated_at TIMESTAMP WITHOUT TIME ZONE,
    sync_token CHARACTER VARYING(255)
);
CREATE TABLE IF NOT EXISTS events(
    id UUID PRIMARY KEY,
    external_uri CHARACTER VARYING(512) NOT NULL,
    name CHARACTER VARYING(255) NOT NULL
);
CREATE TABLE IF NOT EXISTS event_state(
    event_id UUID REFERENCES events(id) NOT NULL,
    updated_at TIMESTAMP WITHOUT TIME ZONE NOT NULL,
    timeslot tsrange
);
