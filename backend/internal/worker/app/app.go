package app

import (
	"CalendarDeviationCalculator/internal/worker/command"
	"CalendarDeviationCalculator/internal/worker/service"
	"context"
	"errors"
	"github.com/segmentio/kafka-go"
	"github.com/sirupsen/logrus"
)

type WorkerApp struct {
	*kafka.Reader
	executor service.CommandExecutor
	logger   *logrus.Logger
}

func (app WorkerApp) Close() error {
	return app.Reader.Close()
}

func (app WorkerApp) Run(ctx context.Context) error {
	for {
		msg, err := app.FetchMessage(ctx)
		if err != nil {
			return err
		}

		err = app.processMessage(ctx, msg)
		if err != nil {
			if errors.Is(err, context.Canceled) {
				return err
			}

			app.logger.WithContext(ctx).WithFields(logrus.Fields{"error": err}).Error("error while processing message, skipping")
		}

		err = app.CommitMessages(ctx, msg)
		if err != nil {
			return err
		}
	}

}

func (app WorkerApp) processMessage(ctx context.Context, msg kafka.Message) error {
	cmd, err := command.ParseJSONCommand(msg.Value)
	if err != nil {
		return err
	}

	return cmd.Accept(ctx, app.executor)
}
