package app

import (
	commonUtils "CalendarDeviationCalculator/internal/common/utils"
	"CalendarDeviationCalculator/internal/worker/service"
	"github.com/gookit/config/v2"
	"github.com/gookit/config/v2/yaml"
)

type PostgresConfig struct {
	ConnectionString commonUtils.SecretString
}

type KafkaConnectionConfig struct {
	BootstrapServers []string
	GroupID          string
}

type Config struct {
	PostgresConfig        PostgresConfig
	KafkaConnectionConfig KafkaConnectionConfig
	WorkerKafkaConfig     service.WorkerKafkaConfig
}

func LoadConfig(filename string) (Config, error) {
	cfg := config.New("api", config.ParseEnv)
	cfg.AddDriver(yaml.Driver)

	err := cfg.LoadFiles(filename)
	if err != nil {
		return Config{}, err
	}

	var configObj Config
	err = cfg.Decode(&configObj)

	return configObj, err
}
