package app

import (
	"CalendarDeviationCalculator/internal/worker/service"
	"database/sql"
	"github.com/segmentio/kafka-go"
	"github.com/sirupsen/logrus"

	_ "github.com/lib/pq"
)

func MakeApp(config Config, logger *logrus.Logger) (WorkerApp, error) {
	reader := kafka.NewReader(kafka.ReaderConfig{
		Brokers:        config.KafkaConnectionConfig.BootstrapServers,
		GroupID:        config.KafkaConnectionConfig.GroupID,
		GroupTopics:    []string{config.WorkerKafkaConfig.Topic},
		CommitInterval: 0,
	})

	dbConnection, err := sql.Open("postgres", string(config.PostgresConfig.ConnectionString))
	if err != nil {
		return WorkerApp{}, err
	}

	return WorkerApp{
		Reader:   reader,
		executor: service.NewCalendarWorkerService(service.NewGoogleClientsFactory(dbConnection), service.NewPersisterService(dbConnection)),
		logger:   logger,
	}, nil
}
