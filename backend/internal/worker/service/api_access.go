package service

import (
	"CalendarDeviationCalculator/internal/api/entity"
	common_entity "CalendarDeviationCalculator/internal/common/entity"
	"CalendarDeviationCalculator/internal/worker/command"
	"context"
	"google.golang.org/api/calendar/v3"
	"time"
)

type GoogleAPIClientFactory interface {
	MakeCalendarService(ctx context.Context, googleAccountId entity.GoogleAccountIdentifier) (*calendar.Service, error)
}

type CalendarWorkerService struct {
	clientFactory GoogleAPIClientFactory
	persister     *PersisterService
}

func NewCalendarWorkerService(clientFactory GoogleAPIClientFactory, persister *PersisterService) *CalendarWorkerService {
	return &CalendarWorkerService{clientFactory: clientFactory, persister: persister}
}

func (svc *CalendarWorkerService) VisitLoadCalendarsCommand(ctx context.Context, cmd *command.LoadCalendarsCommand) error {
	calendarSvc, err := svc.clientFactory.MakeCalendarService(ctx, cmd.GoogleAccountId)
	if err != nil {
		return err
	}

	calendarList, err := calendarSvc.CalendarList.List().Do()
	if err != nil {
		return err
	}

	calendars := make([]common_entity.GoogleCalendar, len(calendarList.Items))
	for i := 0; i < len(calendarList.Items); i++ {
		tz, err := time.LoadLocation(calendarList.Items[i].TimeZone)
		if err != nil {
			return err
		}

		calendars[i] = common_entity.GoogleCalendar{
			Id:       calendarList.Items[i].Id,
			Name:     calendarList.Items[i].Summary,
			Timezone: *tz,
		}
	}

	return svc.persister.PersistCalendars(ctx, cmd.UserId, cmd.GoogleAccountId, calendars)
}
