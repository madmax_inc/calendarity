package service

import (
	"CalendarDeviationCalculator/internal/worker/command"
	"context"
)

type CommandExecutor interface {
	command.Visitor
}

type RateLimitingCommandExecutor struct {
	rateLimiter ExternalRateLimiter
	delegate    CommandExecutor
}

func (r *RateLimitingCommandExecutor) VisitLoadCalendarsCommand(ctx context.Context, cmd *command.LoadCalendarsCommand) error {
	r.delegate.VisitLoadCalendarsCommand(ctx, cmd)
	return nil
}
