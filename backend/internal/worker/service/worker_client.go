package service

import (
	"CalendarDeviationCalculator/internal/worker/command"
	"context"
	"github.com/segmentio/kafka-go"
)

type WorkerKafkaConfig struct {
	Topic string
}

type WorkerClient interface {
	ScheduleCommand(ctx context.Context, cmd command.WorkerCommand) error
}

type KafkaWorkerClient struct {
	writer *kafka.Writer
	config WorkerKafkaConfig
}

func NewKafkaWorkerClient(writer *kafka.Writer, config WorkerKafkaConfig) *KafkaWorkerClient {
	return &KafkaWorkerClient{writer: writer, config: config}
}

func (client *KafkaWorkerClient) ScheduleCommand(ctx context.Context, cmd command.WorkerCommand) error {
	serializer := command.JSONCommandSerializer{}
	err := cmd.Accept(ctx, &serializer)
	if err != nil {
		return err
	}

	result, _ := serializer.GetResult()

	return client.writer.WriteMessages(ctx, kafka.Message{
		Topic: client.config.Topic,
		Value: result,
	})
}
