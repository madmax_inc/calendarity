package service

import "context"

type ExternalRateLimiter interface {
	RunWithinRateLimit(ctx context.Context, action func())
}
