package service

import (
	"CalendarDeviationCalculator/internal/api/entity"
	common_entity "CalendarDeviationCalculator/internal/common/entity"
	"context"
	"database/sql"
	"github.com/google/uuid"
	"github.com/lib/pq"
)

type PersisterService struct {
	db *sql.DB
}

func NewPersisterService(db *sql.DB) *PersisterService {
	return &PersisterService{db: db}
}

func (svc *PersisterService) PersistCalendars(ctx context.Context, userId common_entity.UserIdentifier, googleAccountId entity.GoogleAccountIdentifier, calendars []common_entity.GoogleCalendar) (err error) {
	tx, err := svc.db.BeginTx(ctx, nil)
	if err != nil {
		return err
	}
	defer func() {
		if err != nil {
			tx.Rollback()
		} else {
			tx.Commit()
		}
	}()

	// Find the persisted ones
	googleCalendarIds := make(pq.StringArray, len(calendars))
	for i := 0; i < len(calendars); i++ {
		googleCalendarIds[i] = calendars[i].Id
	}

	internalIds := make(map[string]uuid.UUID)

	presentCalendarsResult, err := tx.QueryContext(ctx, "SELECT google_calendar_id, calendar_id FROM google_calendars WHERE google_account_id = $1 AND google_calendar_id = ANY($2)", googleAccountId, googleCalendarIds)
	if err != nil {
		return err
	}

	for presentCalendarsResult.Next() {
		var (
			googleId   string
			internalId uuid.UUID
		)

		if err = presentCalendarsResult.Scan(&googleId, &internalId); err != nil {
			return err
		}

		internalIds[googleId] = internalId
	}

	//Update the persisted ones, insert the ones, which are not
	for _, calendar := range calendars {
		internalId, ok := internalIds[calendar.Id]

		if ok {
			if _, err = tx.ExecContext(ctx, "UPDATE calendars SET display_name = $1, timezone = $2 WHERE id = $3", calendar.Name, calendar.Timezone.String(), internalId); err != nil {
				return err
			}
		} else {
			row := tx.QueryRowContext(ctx, "INSERT INTO calendars(id, user_id, display_name, timezone, is_tracked) VALUES(gen_random_uuid(), $1, $2, $3, FALSE) RETURNING id", userId, calendar.Name, calendar.Timezone.String())
			if row.Err() != nil {
				return row.Err()
			}

			var calendarId uuid.UUID
			if err = row.Scan(&calendarId); err != nil {
				return err
			}

			if _, err = tx.ExecContext(ctx, "INSERT INTO google_calendars(google_account_id, calendar_id, google_calendar_id) VALUES($1, $2, $3)", googleAccountId, calendarId, calendar.Id); err != nil {
				return err
			}
		}
	}

	return nil
}
