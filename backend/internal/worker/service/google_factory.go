package service

import (
	"CalendarDeviationCalculator/internal/api/entity"
	"context"
	"database/sql"
	"errors"
	"golang.org/x/oauth2"
	"google.golang.org/api/calendar/v3"
	"google.golang.org/api/option"
	"time"
)

type GoogleClientsFactory struct {
	db *sql.DB
}

func NewGoogleClientsFactory(db *sql.DB) *GoogleClientsFactory {
	return &GoogleClientsFactory{db: db}
}

type staticTokenSource struct {
	token *oauth2.Token
}

func (s *staticTokenSource) Token() (*oauth2.Token, error) {
	return s.token, nil
}

func (f *GoogleClientsFactory) MakeCalendarService(ctx context.Context, googleAccountId entity.GoogleAccountIdentifier) (*calendar.Service, error) {
	row := f.db.QueryRowContext(ctx, "SELECT access_token, expires_at FROM google_accounts WHERE id = $1", googleAccountId)
	if err := row.Err(); err != nil {
		return nil, err
	}

	var token oauth2.Token

	if err := row.Scan(&token.AccessToken, &token.Expiry); err != nil {
		return nil, err
	}

	if token.Expiry.Before(time.Now()) {
		return nil, errors.New("access token is expired")
	}

	client := oauth2.NewClient(ctx, &staticTokenSource{token: &token})

	return calendar.NewService(ctx, option.WithHTTPClient(client))
}
