package command

import (
	"CalendarDeviationCalculator/internal/api/entity"
	common_entity "CalendarDeviationCalculator/internal/common/entity"
	"context"
	"encoding/json"
	"errors"
)

const jsonTypeLoadCalendars = "LOAD_CALENDARS"

type jsonCommandHeader struct {
	Type string
}

type jsonCommandFactory interface {
	Command() WorkerCommand
}

type jsonLoadCalendarsCommand struct {
	UserId          common_entity.UserIdentifier
	GoogleAccountId entity.GoogleAccountIdentifier
}

func (cmd jsonLoadCalendarsCommand) Command() WorkerCommand {
	return &LoadCalendarsCommand{UserId: cmd.UserId, GoogleAccountId: cmd.GoogleAccountId}
}

type JSONCommandSerializer struct {
	result []byte
	err    error
}

func (ser *JSONCommandSerializer) GetResult() ([]byte, error) {
	return ser.result, ser.err
}

func (ser *JSONCommandSerializer) serialize(structured interface{}) {
	res, err := json.Marshal(structured)
	ser.result = res
	ser.err = err
}

func (ser *JSONCommandSerializer) VisitLoadCalendarsCommand(ctx context.Context, cmd *LoadCalendarsCommand) error {
	data := struct {
		jsonCommandHeader        `json:",inline"`
		jsonLoadCalendarsCommand `json:",inline"`
	}{
		jsonCommandHeader{Type: jsonTypeLoadCalendars},
		jsonLoadCalendarsCommand{UserId: cmd.UserId, GoogleAccountId: cmd.GoogleAccountId},
	}

	ser.serialize(data)

	return ser.err
}

func parseCommandBody[BodyType jsonCommandFactory](jsonData []byte) (WorkerCommand, error) {
	var result BodyType
	err := json.Unmarshal(jsonData, &result)
	if err != nil {
		return nil, err
	}

	return result.Command(), err
}

func ParseJSONCommand(jsonData []byte) (WorkerCommand, error) {
	var header jsonCommandHeader
	err := json.Unmarshal(jsonData, &header)
	if err != nil {
		return nil, err
	}

	switch header.Type {
	case jsonTypeLoadCalendars:
		return parseCommandBody[jsonLoadCalendarsCommand](jsonData)
	default:
		return nil, errors.New("unknown command")
	}
}
