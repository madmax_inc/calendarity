package command

import (
	"CalendarDeviationCalculator/internal/api/entity"
	common_entity "CalendarDeviationCalculator/internal/common/entity"
	"context"
)

type Visitor interface {
	VisitLoadCalendarsCommand(ctx context.Context, cmd *LoadCalendarsCommand) error
}

type WorkerCommand interface {
	Accept(ctx context.Context, v Visitor) error
}

type LoadCalendarsCommand struct {
	UserId          common_entity.UserIdentifier
	GoogleAccountId entity.GoogleAccountIdentifier
}

func (cmd *LoadCalendarsCommand) Accept(ctx context.Context, v Visitor) error {
	return v.VisitLoadCalendarsCommand(ctx, cmd)
}
