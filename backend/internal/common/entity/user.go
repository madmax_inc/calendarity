package entity

import "github.com/google/uuid"

type UserIdentifier = uuid.UUID
