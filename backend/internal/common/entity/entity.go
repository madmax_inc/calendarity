package entity

import (
	"github.com/google/uuid"
	"time"
)

type TimeSpan struct {
	Begin time.Time
	End   time.Time
}

type Calendar struct {
	Id          uuid.UUID      `json:"id"`
	ExternalURI *string        `json:"externalUri,omitempty"`
	Name        string         `json:"name"`
	Timezone    *time.Location `json:"timezone"`
	IsTracked   bool           `json:"isTracked"`
}

type Event struct {
	Id          uuid.UUID
	Name        string
	Description string
	UpdatedAt   time.Time
	ScheduledAt TimeSpan
}
