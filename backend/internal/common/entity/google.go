package entity

import (
	"golang.org/x/oauth2"
	"time"
)

type GoogleOAuthAccessToken struct {
	*oauth2.Token
}

type GoogleCalendar struct {
	Id       string
	Name     string
	Timezone time.Location
}

type GoogleEvent struct {
	Event
	GoogleEventId string
}

type GoogleCalendarSpan struct {
	TimeSpan
	SyncToken string
}
