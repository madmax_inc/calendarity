package utils

import (
	"github.com/stretchr/testify/assert"
	"io"
	"os"
	"testing"
)

func TestSecretValueRaw(t *testing.T) {
	val := SecretString("foo")

	rawVal, err := val.Resolve()
	assert.NoError(t, err)
	assert.Equal(t, "foo", rawVal)
}

func TestSecretValueFile(t *testing.T) {
	fp, err := os.CreateTemp("", "test")
	assert.NoError(t, err)
	defer fp.Close()

	_, err = io.WriteString(fp, "foobarbaz")
	assert.NoError(t, err)

	val := SecretString("%{file://" + fp.Name() + "}")

	rawVal, err := val.Resolve()
	assert.NoError(t, err)
	assert.Equal(t, "foobarbaz", rawVal)
}

func TestSecretValueNoFile(t *testing.T) {
	val := SecretString("%{file:///tmp/testable}")

	_, err := val.Resolve()
	assert.Error(t, err)
}

func TestSecretValueNested(t *testing.T) {
	fp, err := os.CreateTemp("", "test")
	assert.NoError(t, err)
	defer fp.Close()

	_, err = io.WriteString(fp, "foobarbaz")
	assert.NoError(t, err)

	type Inner struct {
		Rty    string
		Qwerty SecretString
	}
	var testable = struct {
		Foo   int
		bar   int
		Baz   string
		Qwe   SecretString
		Inner Inner
		inner Inner
	}{
		Foo: 1,
		bar: 2,
		Baz: "123",
		Qwe: SecretString("qwerty"),
		Inner: Inner{
			Rty:    "qwe",
			Qwerty: SecretString("%{file://" + fp.Name() + "}"),
		},
		inner: Inner{
			Rty:    "qwe",
			Qwerty: SecretString("%{file://" + fp.Name() + "}"),
		},
	}

	err = ResolveAll(&testable)
	assert.NoError(t, err)
	assert.Equal(t, "qwerty", string(testable.Qwe))
	assert.Equal(t, "foobarbaz", string(testable.Inner.Qwerty))
	assert.NotEqual(t, "foobarbaz", string(testable.inner.Qwerty))
}
