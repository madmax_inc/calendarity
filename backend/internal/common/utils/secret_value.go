package utils

import (
	"errors"
	"io"
	"net/url"
	"os"
	"reflect"
	"strings"
)

const specialValueBegins = "%{"
const specialValueEnds = "}"

type SecretString string

func (str SecretString) Resolve() (string, error) {
	if !strings.HasPrefix(string(str), specialValueBegins) || !strings.HasSuffix(string(str), specialValueEnds) {
		return string(str), nil
	}

	ref := str[len(specialValueBegins) : len(str)-len(specialValueEnds)]

	return parseRef(string(ref))
}

func ResolveAll(obj any) error {
	objType := reflect.ValueOf(obj)
	if objType.Kind() != reflect.Ptr {
		return nil
	}

	objConcreteType := reflect.Indirect(objType)
	if objConcreteType.Kind() != reflect.Struct {
		return nil
	}

	for i := 0; i < objConcreteType.NumField(); i++ {
		fieldRef := objConcreteType.Field(i)
		if !fieldRef.CanInterface() {
			continue
		}
		fieldAddr := fieldRef.Addr().Interface()
		if val, ok := fieldAddr.(*SecretString); ok {
			newVal, err := val.Resolve()
			if err != nil {
				return err
			}
			newSecret := SecretString(newVal)

			fieldRef.Set(reflect.ValueOf(newSecret))
			continue
		}

		err := ResolveAll(fieldAddr)
		if err != nil {
			return err
		}
	}

	return nil
}

func parseRef(reference string) (string, error) {
	urlObj, err := url.Parse(reference)
	if err != nil {
		return "", err
	}

	if urlObj.Scheme != "file" {
		return "", errors.New("unknown scheme")
	}

	fp, err := os.Open(urlObj.Path)
	if err != nil {
		return "", err
	}

	defer fp.Close()

	content, err := io.ReadAll(fp)
	if err != nil {
		return "", err
	}

	if len(content) == 0 {
		return "", errors.New("value was not provided")
	}

	return string(content), nil
}
