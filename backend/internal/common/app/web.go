package app

import (
	"context"
	"errors"
	"fmt"
	"net"
	"net/http"
	"net/url"
	"testing"

	"github.com/phayes/freeport"

	"github.com/vitorsalgado/mocha/v3"
)

type WebApplication struct {
	*http.Server
}

type TestWebContext struct {
	Client *http.Client
	Mocha  *mocha.Mocha
}

func NewWebApplication(addr string, handler http.Handler) *WebApplication {
	return &WebApplication{
		&http.Server{Addr: addr, Handler: handler},
	}
}

func NewTestWebApplication(handler http.Handler) (*WebApplication, error) {
	port, err := freeport.GetFreePort()
	if err != nil {
		return nil, err
	}

	return NewWebApplication(fmt.Sprint(":", port), handler), nil
}

func (app *WebApplication) Run(ctx context.Context, readyChan chan interface{}) error {
	if readyChan == nil {
		readyChan = make(chan interface{}, 0)
	}

	listener, err := net.Listen("tcp", app.Addr)
	if err != nil {
		close(readyChan)
		return err
	}
	close(readyChan)

	errChan := make(chan error, 1)

	go func() {
		defer close(errChan)
		if err := app.Serve(listener); err != nil {
			if !errors.Is(err, http.ErrServerClosed) {
				errChan <- err
			}
		}
	}()

	var result error

	select {
	case <-ctx.Done():
		app.Shutdown(ctx)
	case result = <-errChan:
	}

	<-errChan

	return result
}

type testTransportWrapper struct {
	serverAddr string
	delegate   http.RoundTripper
}

func (wrapper *testTransportWrapper) RoundTrip(req *http.Request) (*http.Response, error) {
	clonedReq := req.Clone(req.Context())

	newUrl, err := url.Parse(wrapper.serverAddr + clonedReq.URL.String())
	if err != nil {
		return nil, err
	}
	clonedReq.URL = newUrl

	return wrapper.delegate.RoundTrip(clonedReq)
}

func BootstrapWebApp(t *testing.T, app *WebApplication) *http.Client {
	ctx, cancel := context.WithCancel(context.Background())
	readyChan := make(chan interface{}, 0)

	go func() {
		if err := app.Run(ctx, readyChan); err != nil {
			t.Fatal(err)
		}
	}()
	<-readyChan
	t.Cleanup(cancel)

	return &http.Client{
		Transport: &testTransportWrapper{
			serverAddr: "http://localhost" + app.Addr,
			delegate:   http.DefaultTransport,
		},
		CheckRedirect: func(req *http.Request, via []*http.Request) error {
			return http.ErrUseLastResponse
		},
	}
}
