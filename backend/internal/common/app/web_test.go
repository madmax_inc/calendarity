package app

import (
	"github.com/stretchr/testify/assert"
	"io"
	"net/http"
	"testing"
)

func handler(resp http.ResponseWriter, req *http.Request) {
	resp.WriteHeader(http.StatusOK)
	resp.Write([]byte("That's all, folks! - " + req.URL.Path))
}

func TestSimpleWebApp(t *testing.T) {
	app, err := NewTestWebApplication(http.HandlerFunc(handler))
	assert.NoError(t, err)
	client := BootstrapWebApp(t, app)

	resp, err := client.Get("/foo/bar/baz")
	assert.NoError(t, err)

	assert.Equal(t, 200, resp.StatusCode)
	body, err := io.ReadAll(resp.Body)
	assert.NoError(t, err)
	assert.Equal(t, "That's all, folks! - /foo/bar/baz", string(body))
}
