package service

import (
	"CalendarDeviationCalculator/internal/common/entity"
	"context"
	"database/sql"
	"errors"
	"github.com/google/uuid"
	"time"
)

/*
CREATE TABLE IF NOT EXISTS calendars(
    id UUID PRIMARY KEY,
    user_id UUID REFERENCES users(id) NOT NULL,
    display_name CHARACTER VARYING(255) NOT NULL,
    timezone CHARACTER VARYING(128) NOT NULL,
    is_tracked BOOLEAN NOT NULL
);
CREATE TABLE IF NOT EXISTS google_calendars(
    google_account_id CHARACTER VARYING(255) REFERENCES google_accounts(id) NOT NULL,
    calendar_id UUID REFERENCES calendars(id) NOT NULL,
    google_calendar_id CHARACTER VARYING(255) NOT NULL
);
*/

const getCalendarsQuery = `
SELECT u.calendars_refreshing, u.calendars_updated_at, c.id, c.display_name, c.timezone, c.is_tracked, COALESCE('google://' || gc.google_calendar_id) AS external_uri
FROM users u
LEFT JOIN calendars c ON u.id = c.user_id  
LEFT JOIN google_calendars gc ON c.id = gc.calendar_id
WHERE u.id = $1
`

var ErrNoSuchUser = errors.New("no such user")

type UserMeta struct {
	CalendarsRefreshing bool
	CalendarsUpdatedAt  *time.Time
}

type UserCalendarsData struct {
	UserMeta
	Calendars []entity.Calendar
}

type CalendarService interface {
	ListCalendars(ctx context.Context, userId entity.UserIdentifier) (UserCalendarsData, error)
}

type scannableZone struct{ *time.Location }

func (tz *scannableZone) Scan(value any) error {
	stringTz, ok := value.(string)
	if !ok {
		return errors.New("not a string")
	}

	location, err := time.LoadLocation(stringTz)
	if err != nil {
		return err
	}

	tz.Location = location

	return nil
}

type calendarOptionalResultRow struct {
	UserMeta
	Id          *uuid.UUID
	DisplayName *string
	TimeZone    *scannableZone
	IsTracked   *bool
	ExternalUri *string
}

func (optRow calendarOptionalResultRow) Must() calendarResultRow {
	return calendarResultRow{
		Id:          *optRow.Id,
		DisplayName: *optRow.DisplayName,
		TimeZone:    *optRow.TimeZone,
		IsTracked:   *optRow.IsTracked,
		ExternalUri: optRow.ExternalUri,
	}
}

type calendarResultRow struct {
	Id          uuid.UUID
	DisplayName string
	TimeZone    scannableZone
	IsTracked   bool
	ExternalUri *string
}

type SQLRepoCalendarService struct {
	db *sql.DB
}

func NewSQLRepoCalendarService(db *sql.DB) *SQLRepoCalendarService {
	return &SQLRepoCalendarService{db: db}
}

func (svc *SQLRepoCalendarService) ListCalendars(ctx context.Context, userId entity.UserIdentifier) (UserCalendarsData, error) {
	rows, err := svc.db.QueryContext(
		ctx,
		getCalendarsQuery,
		userId,
	)

	if err != nil {
		return UserCalendarsData{}, err
	}

	result := UserCalendarsData{Calendars: []entity.Calendar{}}
	var resultRow calendarOptionalResultRow

	noUser := true
	for rows.Next() {
		err = rows.Scan(&resultRow.CalendarsRefreshing, &resultRow.CalendarsUpdatedAt, &resultRow.Id, &resultRow.DisplayName, &resultRow.TimeZone, &resultRow.IsTracked, &resultRow.ExternalUri)
		if err != nil {
			return UserCalendarsData{}, err
		}

		if noUser {
			noUser = false
			result.UserMeta = resultRow.UserMeta
		}

		if resultRow.Id == nil {
			break
		}
		result.Calendars = append(result.Calendars, adoptCalendar(resultRow.Must()))
	}

	if noUser {
		return UserCalendarsData{}, ErrNoSuchUser
	}

	return result, nil
}

func adoptCalendar(row calendarResultRow) entity.Calendar {
	return entity.Calendar{
		Id:          row.Id,
		ExternalURI: row.ExternalUri,
		Name:        row.DisplayName,
		Timezone:    row.TimeZone.Location,
		IsTracked:   row.IsTracked,
	}
}
