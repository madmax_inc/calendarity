package action

type UpdaterAction interface {
	Accept(visitor UpdaterActionVisitor)
}

type LoadSpanAction struct {
}

func (action *LoadSpanAction) Accept(visitor UpdaterActionVisitor) {
	visitor.VisitLoadSpanAction(action)
}

type CheckSpanAction struct {
}

func (action *CheckSpanAction) Accept(visitor UpdaterActionVisitor) {
	visitor.VisitCheckSpanAction(action)
}
