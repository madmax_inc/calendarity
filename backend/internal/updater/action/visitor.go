package action

type UpdaterActionVisitor interface {
	VisitCheckSpanAction(action *CheckSpanAction)
	VisitLoadSpanAction(action *LoadSpanAction)
}
