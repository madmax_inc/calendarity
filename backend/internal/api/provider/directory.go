package provider

import (
	"CalendarDeviationCalculator/internal/api/utils"
	"golang.org/x/oauth2"
)

type ProvidersDirectory struct {
	providers map[string]*OAuthAuthenticationProvider
}

func NewProvidersDirectory() *ProvidersDirectory {
	return &ProvidersDirectory{providers: make(map[string]*OAuthAuthenticationProvider)}
}

func NewSupportedProvidersDirectory(googleConfig utils.OAuthConfig) *ProvidersDirectory {
	return NewProvidersDirectory().WithProvider("google", NewOAuthAuthenticationProvider(
		googleConfig.Convert(),
		[]oauth2.AuthCodeOption{oauth2.AccessTypeOffline},
		utils.NewUserInfoOpenIDGetter(googleConfig.UserInfoEndpoint),
	))
}

func (dir *ProvidersDirectory) WithProvider(slug string, provider *OAuthAuthenticationProvider) *ProvidersDirectory {
	dir.providers[slug] = provider
	return dir
}

func (dir *ProvidersDirectory) Resolve(slug string) *OAuthAuthenticationProvider {
	return dir.providers[slug]
}
