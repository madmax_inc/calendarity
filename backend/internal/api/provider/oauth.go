package provider

import (
	"CalendarDeviationCalculator/internal/api/utils"
	"context"
	"golang.org/x/oauth2"
)

type OAuthAuthenticationProvider struct {
	config          *oauth2.Config
	authCodeOptions []oauth2.AuthCodeOption
	userInfoGetter  utils.UserInfoGetter
}

func NewOAuthAuthenticationProvider(config *oauth2.Config, authCodeOptions []oauth2.AuthCodeOption, userInfoGetter utils.UserInfoGetter) *OAuthAuthenticationProvider {
	return &OAuthAuthenticationProvider{config: config, authCodeOptions: authCodeOptions, userInfoGetter: userInfoGetter}
}

func (provider *OAuthAuthenticationProvider) MakeAuthURL(state string) string {
	return provider.config.AuthCodeURL(state, provider.authCodeOptions...)
}

func (provider *OAuthAuthenticationProvider) ExchangeCode(ctx context.Context, code string) (*oauth2.Token, error) {
	return utils.Exchange(ctx, provider.config, code, nil)
}

func (provider *OAuthAuthenticationProvider) UserInfo(ctx context.Context, token string) (utils.UserInfo, error) {
	return provider.userInfoGetter.GetUserInfo(ctx, token)
}
