package utils

import (
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
)

type dummyWriter struct{}

func (w *dummyWriter) Write(p []byte) (n int, err error) {
	return len(p), nil
}

func MakeDummyLogger() *logrus.Logger {
	dummyLogger := logrus.New()
	dummyLogger.Out = &dummyWriter{}
	return dummyLogger
}

func MakeLoggingMiddleware(logger *logrus.Logger) gin.HandlerFunc {
	return func(c *gin.Context) {
		c.Next()

		if !c.IsAborted() {
			return
		}

		logger.WithFields(map[string]interface{}{
			"status": c.Writer.Status(),
			"errors": c.Errors,
		}).Debug("handler aborted with errors")
	}
}
