package utils

import (
	"CalendarDeviationCalculator/internal/common/utils"
	"context"
	"encoding/json"
	"errors"
	"golang.org/x/oauth2"
	"net/http"
	"net/url"
)

const WellKnownRelativePath = ".well-known/openid-configuration"

type OAuthConfig struct {
	ClientID         utils.SecretString
	ClientSecret     utils.SecretString
	Endpoint         oauth2.Endpoint
	RedirectURL      string
	Scopes           []string
	CallbackUrl      string
	UserInfoEndpoint string
	WellKnownPrefix  string
}

func (cfg *OAuthConfig) Convert() *oauth2.Config {
	return &oauth2.Config{
		ClientID:     string(cfg.ClientID),
		ClientSecret: string(cfg.ClientSecret),
		Endpoint:     cfg.Endpoint,
		RedirectURL:  cfg.RedirectURL,
		Scopes:       cfg.Scopes,
	}
}

type OAuthEndpoints struct {
	AuthorizationEndpoint string `json:"authorization_endpoint"`
	TokenEndpoint         string `json:"token_endpoint"`
	UserInfoEndpoint      string `json:"userinfo_endpoint"`
}

type UserInfo struct {
	Sub  string `json:"sub"`
	Name string `json:"name"`
}

func (cfg *OAuthConfig) GetOAuthEndpoints(ctx context.Context, client *http.Client) (endpoints OAuthEndpoints, err error) {
	if client == nil {
		client = http.DefaultClient
	}

	endpointUrl, err := wellKnownEndpoint(cfg.WellKnownPrefix)
	if err != nil {
		return
	}

	req, err := http.NewRequest(
		http.MethodGet,
		endpointUrl,
		nil,
	)
	if err != nil {
		return
	}

	response, err := client.Do(req.WithContext(ctx))
	if err != nil {
		return
	}

	if response.StatusCode < 200 || response.StatusCode > 299 {
		err = errors.New("unexpected status code")
		return
	}

	defer response.Body.Close()

	err = json.NewDecoder(response.Body).Decode(&endpoints)
	if err != nil {
		return
	}

	return
}

func (cfg *OAuthConfig) UpdateOAuthEndpoints(ctx context.Context, client *http.Client) error {
	endpoints, err := cfg.GetOAuthEndpoints(ctx, client)
	if err != nil {
		return err
	}

	cfg.Endpoint.AuthURL = endpoints.AuthorizationEndpoint
	cfg.Endpoint.TokenURL = endpoints.TokenEndpoint
	cfg.UserInfoEndpoint = endpoints.UserInfoEndpoint

	return nil
}

func Exchange(ctx context.Context, config *oauth2.Config, code string, client *http.Client) (*oauth2.Token, error) {
	if client != nil {
		ctx = context.WithValue(ctx, oauth2.HTTPClient, client)
	}

	return config.Exchange(ctx, code)
}

type UserInfoGetter interface {
	GetUserInfo(ctx context.Context, accessToken string) (UserInfo, error)
}

type UserInfoOpenIDGetter struct {
	userInfoEndpoint string
	client           *http.Client
}

func NewUserInfoOpenIDGetter(userInfoEndpoint string) *UserInfoOpenIDGetter {
	return &UserInfoOpenIDGetter{userInfoEndpoint: userInfoEndpoint, client: http.DefaultClient}
}

func (getter *UserInfoOpenIDGetter) WithClient(client *http.Client) *UserInfoOpenIDGetter {
	getter.client = client
	return getter
}

func (getter *UserInfoOpenIDGetter) GetUserInfo(ctx context.Context, accessToken string) (userInfo UserInfo, err error) {
	endpointUrl, err := userInfoEndpoint(getter.userInfoEndpoint, accessToken)
	if err != nil {
		return
	}

	req, err := http.NewRequest(
		http.MethodGet,
		endpointUrl,
		nil,
	)
	if err != nil {
		return
	}

	response, err := getter.client.Do(req.WithContext(ctx))
	if err != nil {
		return
	}

	if response.StatusCode < 200 || response.StatusCode > 299 {
		return UserInfo{}, errors.New("unexpected status code")
	}

	defer response.Body.Close()

	err = json.NewDecoder(response.Body).Decode(&userInfo)

	return
}

func wellKnownEndpoint(prefix string) (string, error) {
	urlObj, err := url.Parse(prefix)
	if err != nil {
		return "", err
	}

	urlObj.Path += WellKnownRelativePath

	return urlObj.String(), nil
}

func userInfoEndpoint(path string, accessToken string) (string, error) {
	urlObj, err := url.Parse(path)
	if err != nil {
		return "", err
	}

	query := urlObj.Query()
	query.Add("access_token", accessToken)
	urlObj.RawQuery = query.Encode()

	return urlObj.String(), nil
}
