package dto

import "CalendarDeviationCalculator/internal/common/entity"

type ListCalendarsDTO struct {
	IsOutdated bool              `json:"isOutdated"`
	Calendars  []entity.Calendar `json:"calendars"`
}
