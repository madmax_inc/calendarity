package entity

type CountableMixin struct {
	Total int
}

type PaginationTokens struct {
	Previous *string
	Next     *string
}

type PaginationMixin struct {
	PaginationTokens *PaginationTokens
}
