package entity

import "CalendarDeviationCalculator/internal/common/entity"

type UserMeta struct {
	Name string
}

type User struct {
	Id entity.UserIdentifier
	UserMeta
}
