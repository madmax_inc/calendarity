package entity

import "CalendarDeviationCalculator/internal/common/entity"

type GoogleAccountIdentifier string

type AssociatedGoogleAccountMeta struct {
	AccountId   GoogleAccountIdentifier
	DisplayName string
}

type AssociatedGoogleAccount struct {
	AssociatedGoogleAccountMeta
	AccessToken entity.GoogleOAuthAccessToken
}
