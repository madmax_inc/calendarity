package controller

import (
	"github.com/gin-gonic/gin"
	"io"
	"net/http"
)

const sseContentType = "text/events-stream"

type UpdatesController struct {
}

func (controller *UpdatesController) Subscribe(context *gin.Context) {
	// SSE events entry point
	context.Header("Content-Type", sseContentType)
	context.Status(http.StatusOK)
	context.Stream(func(writer io.Writer) bool {
		return true
	})
}
