package controller

import (
	web_entity "CalendarDeviationCalculator/internal/api/entity"
	"CalendarDeviationCalculator/internal/api/provider"
	"CalendarDeviationCalculator/internal/api/security"
	"CalendarDeviationCalculator/internal/api/service"
	"CalendarDeviationCalculator/internal/common/entity"
	"CalendarDeviationCalculator/internal/worker/command"
	worker_service "CalendarDeviationCalculator/internal/worker/service"
	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"net/http"
	"net/url"
)

type OAuthController struct {
	providersDirectory    *provider.ProvidersDirectory
	tokenAuthority        security.TokenAuthority
	authCallback          AppAuthenticationCallback
	authService           service.AuthorizationService
	authUserMangleService service.AuthorizationStateManglerService
	workerClient          worker_service.WorkerClient
}

func NewOAuthController(
	providersDirectory *provider.ProvidersDirectory,
	tokenAuthority security.TokenAuthority,
	authCallback AppAuthenticationCallback,
	authService service.AuthorizationService,
	authUserMangleService service.AuthorizationStateManglerService,
	workerClient worker_service.WorkerClient,
) *OAuthController {
	return &OAuthController{
		providersDirectory:    providersDirectory,
		tokenAuthority:        tokenAuthority,
		authCallback:          authCallback,
		authService:           authService,
		authUserMangleService: authUserMangleService,
		workerClient:          workerClient,
	}
}

type AppAuthenticationCallback struct {
	BaseUrl string
}

func (cb *AppAuthenticationCallback) MakeHappyURL(jwt string) (string, error) {
	redirectUrl, err := url.Parse(cb.BaseUrl)
	if err != nil {
		return "", err
	}

	redirectUrl.Fragment = url.Values{
		"status": []string{"SUCCESS"},
		"token":  []string{jwt},
	}.Encode()

	return redirectUrl.String(), nil
}

func (cb *AppAuthenticationCallback) MakeFailureURL(reason string) (string, error) {
	redirectUrl, err := url.Parse(cb.BaseUrl)
	if err != nil {
		return "", err
	}

	redirectUrl.Fragment = url.Values{
		"status": []string{"FAILURE"},
		"reason": []string{reason},
	}.Encode()

	return redirectUrl.String(), nil
}

func (controller *OAuthController) InitializeLogin(context *gin.Context) {
	externalProvider := controller.providersDirectory.Resolve(context.Param("provider"))
	if externalProvider == nil {
		context.Status(http.StatusNotFound)

		return
	}

	loginUrl := externalProvider.MakeAuthURL("")

	context.Redirect(http.StatusSeeOther, loginUrl)
}

func (controller *OAuthController) InitializeBindAccount(context *gin.Context) {
	userId, err := uuid.Parse(context.Param("userId"))
	if err != nil {
		context.Status(http.StatusBadRequest)

		return
	}

	state, err := controller.authUserMangleService.MangleUserId(context, userId)
	if err != nil {
		context.AbortWithError(http.StatusInternalServerError, err).SetType(gin.ErrorTypePrivate)

		return
	}

	externalProvider := controller.providersDirectory.Resolve(context.Param("provider"))
	if externalProvider == nil {
		context.Status(http.StatusNotFound)

		return
	}

	loginUrl := externalProvider.MakeAuthURL(state)

	context.Redirect(http.StatusSeeOther, loginUrl)
}

func (controller *OAuthController) HandleCallback(context *gin.Context) {
	externalProvider := controller.providersDirectory.Resolve(context.Param("provider"))
	if externalProvider == nil {
		context.Status(http.StatusNotFound)

		return
	}

	redirectUrl, cmd, err := func() (url string, cmd command.WorkerCommand, err error) {
		code := context.Query("code")
		state := context.Query("state")

		issuedToken := ""
		var userId *entity.UserIdentifier

		if code == "" {
			url, err = controller.authCallback.MakeFailureURL("unauthorized")
			return
		}

		if state != "" {
			var boundUserId entity.UserIdentifier
			boundUserId, err = controller.authUserMangleService.DemangleUserId(context, state)
			userId = &boundUserId

			if err != nil {
				url, err = controller.authCallback.MakeFailureURL("malformed state")

				return
			}
		}

		token, err := externalProvider.ExchangeCode(context, code)
		if err != nil {
			url, err = controller.authCallback.MakeFailureURL("bad oauth code")
			return
		}

		userInfo, err := externalProvider.UserInfo(context, token.AccessToken)
		if err != nil {
			url, err = controller.authCallback.MakeFailureURL("invalid oauth code")
			return
		}

		googleAccount := web_entity.AssociatedGoogleAccount{
			AssociatedGoogleAccountMeta: web_entity.AssociatedGoogleAccountMeta{
				AccountId:   web_entity.GoogleAccountIdentifier(userInfo.Sub),
				DisplayName: userInfo.Name,
			},
			AccessToken: entity.GoogleOAuthAccessToken{Token: token},
		}

		var isCreated bool
		var result service.ConditionalAuthResult

		if userId != nil {
			result, err = controller.authService.BindGoogleAccount(context, *userId, googleAccount)
			if err != nil {
				url, err = controller.authCallback.MakeFailureURL(err.Error())
				return
			}
			isCreated = result.IsCreated
		} else {
			result, err = controller.authService.UpsertUser(context, web_entity.UserMeta{Name: userInfo.Name}, googleAccount)
			userId = &result.UserId
			if err != nil {
				url, err = controller.authCallback.MakeFailureURL(err.Error())
				return
			}
			isCreated = result.IsCreated
		}

		if isCreated {
			cmd = &command.LoadCalendarsCommand{UserId: *userId, GoogleAccountId: googleAccount.AccountId}
		}

		issuedToken, err = security.IssueUserToken(&controller.tokenAuthority, *userId, token.Expiry)
		if err != nil {
			return
		}

		url, err = controller.authCallback.MakeHappyURL(issuedToken)

		return
	}()

	if err != nil {
		context.AbortWithError(http.StatusInternalServerError, err)

		return
	}

	if cmd != nil {
		err = controller.workerClient.ScheduleCommand(context, cmd)

		if err != nil {
			context.AbortWithError(http.StatusInternalServerError, err)

			return
		}
	}

	context.Redirect(http.StatusMovedPermanently, redirectUrl)
}
