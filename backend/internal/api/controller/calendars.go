package controller

import (
	"CalendarDeviationCalculator/internal/api/dto"
	"CalendarDeviationCalculator/internal/common/service"
	"errors"
	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"net/http"
)

type CalendarsController struct {
	calendarsSvc service.CalendarService
}

func NewCalendarsController(calendarsSvc service.CalendarService) *CalendarsController {
	return &CalendarsController{calendarsSvc: calendarsSvc}
}

func (controller *CalendarsController) GetUserCalendars(context *gin.Context) {
	userId, err := uuid.Parse(context.Param("userId"))
	if err != nil {
		context.AbortWithError(http.StatusBadRequest, err).SetType(gin.ErrorTypePrivate)

		return
	}

	calendars, err := controller.calendarsSvc.ListCalendars(context, userId)
	if err != nil {
		if errors.Is(err, service.ErrNoSuchUser) {
			context.Status(http.StatusNotFound)
		} else {
			context.AbortWithError(http.StatusInternalServerError, err).SetType(gin.ErrorTypePrivate)
		}

		return
	}

	response := dto.ListCalendarsDTO{
		IsOutdated: calendars.CalendarsRefreshing || calendars.CalendarsUpdatedAt == nil,
		Calendars:  calendars.Calendars,
	}

	context.JSON(http.StatusOK, response)
}
