package security

type SecretVault interface {
	GetValue() (string, error)
}

type StaticSecretVault struct {
	value string
}

func NewStaticSecretVault(value string) *StaticSecretVault {
	return &StaticSecretVault{value: value}
}

func (vault *StaticSecretVault) GetValue() (string, error) {
	return vault.value, nil
}
