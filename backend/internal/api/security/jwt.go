package security

import (
	"CalendarDeviationCalculator/internal/common/entity"
	"github.com/golang-jwt/jwt/v5"
	"time"
)

type TokenAuthority struct {
	signingMethod    jwt.SigningMethod
	signingSecret    SecretVault
	validationSecret SecretVault
}

func NewTokenAuthority(signingMethod jwt.SigningMethod, signingSecret SecretVault, validationSecret SecretVault) TokenAuthority {
	return TokenAuthority{signingMethod: signingMethod, signingSecret: signingSecret,
		validationSecret: validationSecret}
}

func (authority TokenAuthority) CheckJWT(token string) (*jwt.Token, error) {
	decodedToken, err := jwt.Parse(
		token,
		func(*jwt.Token) (interface{}, error) { return authority.validationSecret.GetValue() },
		jwt.WithValidMethods([]string{authority.signingMethod.Alg()}),
	)
	if err != nil {
		return nil, err
	}

	return decodedToken, nil
}

func (authority TokenAuthority) IssueJWT(claims jwt.Claims) (string, error) {
	secret, err := authority.signingSecret.GetValue()
	if err != nil {
		return "", err
	}

	return jwt.NewWithClaims(authority.signingMethod, claims).SignedString([]byte(secret))
}

func IssueUserToken(authority *TokenAuthority, userId entity.UserIdentifier, expiresAt time.Time) (string, error) {
	return authority.IssueJWT(jwt.RegisteredClaims{Subject: userId.String(), ExpiresAt: jwt.NewNumericDate(expiresAt)})
}
