package app

import (
	"CalendarDeviationCalculator/internal/api/controller"
	"CalendarDeviationCalculator/internal/api/provider"
	"CalendarDeviationCalculator/internal/api/security"
	"CalendarDeviationCalculator/internal/api/service"
	"CalendarDeviationCalculator/internal/api/utils"
	worker_service "CalendarDeviationCalculator/internal/worker/service"
	"context"
	"github.com/segmentio/kafka-go"
	"github.com/sirupsen/logrus"

	commonService "CalendarDeviationCalculator/internal/common/service"
	commonUtils "CalendarDeviationCalculator/internal/common/utils"

	"database/sql"
	"github.com/golang-jwt/jwt/v5"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"

	_ "github.com/lib/pq"
)

type CalendarDeviationAPIApp struct {
	http.Handler
	kafkaWriter *kafka.Writer
}

func (app CalendarDeviationAPIApp) Close() error {
	return app.kafkaWriter.Close()
}

func registerControllers(
	engine *gin.Engine,
	oauthController *controller.OAuthController,
	calendarsController *controller.CalendarsController,
) {
	engine.GET("/auth/:provider", oauthController.InitializeLogin)
	engine.GET("/users/:userId//boundAccounts/:provider/initiate", oauthController.InitializeBindAccount)
	engine.GET("/auth/:provider/callback", oauthController.HandleCallback)

	engine.GET("/users/:userId/calendars", calendarsController.GetUserCalendars)
}

func MakeApp(config Config, logger *logrus.Logger) (*CalendarDeviationAPIApp, error) {
	err := commonUtils.ResolveAll(&config)
	if err != nil {
		return nil, err
	}

	tokenSigner := security.NewTokenAuthority(
		jwt.SigningMethodHS384,
		security.NewStaticSecretVault("foo"),
		security.NewStaticSecretVault("foo"),
	)

	dbConnection, err := sql.Open("postgres", string(config.PostgresConfig.ConnectionString))
	if err != nil {
		return nil, err
	}

	authService := service.NewSQLRepoAuthorizationService(
		dbConnection,
	)
	authMangleService := service.NewJWTManglerService(
		security.NewTokenAuthority(
			jwt.SigningMethodHS256,
			security.NewStaticSecretVault("bar"),
			security.NewStaticSecretVault("bar"),
		),
		time.Minute*15,
	)

	if config.OAuthConfig.UserInfoEndpoint == "" && config.OAuthConfig.Endpoint.AuthURL == "" && config.OAuthConfig.Endpoint.TokenURL == "" {
		err = config.OAuthConfig.UpdateOAuthEndpoints(context.Background(), nil)

		if err != nil {
			return nil, err
		}
	}

	externalProvidersDirectory := provider.NewSupportedProvidersDirectory(config.OAuthConfig)

	kafkaWriter := &kafka.Writer{
		Addr:                   kafka.TCP(config.KafkaConnectionConfig.BootstrapServers...),
		Async:                  true,
		AllowAutoTopicCreation: false,
	}
	workerClient := worker_service.NewKafkaWorkerClient(kafkaWriter, config.WorkerKafkaConfig)

	oauthController := controller.NewOAuthController(
		externalProvidersDirectory,
		tokenSigner,
		controller.AppAuthenticationCallback{
			BaseUrl: config.OAuthConfig.CallbackUrl,
		},
		authService,
		authMangleService,
		workerClient,
	)
	calendarsController := controller.NewCalendarsController(
		commonService.NewSQLRepoCalendarService(dbConnection),
	)

	engine := gin.New()
	engine.Use(utils.MakeLoggingMiddleware(logger))

	registerControllers(engine, oauthController, calendarsController)

	return &CalendarDeviationAPIApp{Handler: engine, kafkaWriter: kafkaWriter}, nil
}
