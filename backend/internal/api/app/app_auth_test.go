package app

import (
	"CalendarDeviationCalculator/internal/api/utils"
	"CalendarDeviationCalculator/internal/worker/command"
	"context"
	"github.com/golang-jwt/jwt/v5"
	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
	"github.com/vitorsalgado/mocha/v3"
	"github.com/vitorsalgado/mocha/v3/expect"
	"github.com/vitorsalgado/mocha/v3/reply"
	"golang.org/x/oauth2"
	"net/url"
	"testing"
)

func TestApp_OAuthRedirect(t *testing.T) {
	application := MakeTestApplication(t, Config{
		OAuthConfig: utils.OAuthConfig{
			ClientID:     "client-id",
			ClientSecret: "client-secret",
			Endpoint: oauth2.Endpoint{
				AuthURL:   "http://oauth",
				AuthStyle: oauth2.AuthStyleInParams,
			},
			RedirectURL: "http://app/",
			Scopes:      []string{"calendar"},
		},
	})

	resp, err := application.Client.Get("/auth/google")
	assert.NoError(t, err)
	assert.Equal(t, 303, resp.StatusCode)

	parsedOAuthURL, err := url.Parse(resp.Header.Get("Location"))
	assert.NoError(t, err)

	assert.Equal(t, "client-id", parsedOAuthURL.Query().Get("client_id"))
	assert.Equal(t, "http://app/", parsedOAuthURL.Query().Get("redirect_uri"))
	assert.Equal(t, "code", parsedOAuthURL.Query().Get("response_type"))
	assert.Equal(t, "calendar", parsedOAuthURL.Query().Get("scope"))
}

func TestApp_OAuthRedirect_Bind(t *testing.T) {
	application := MakeTestApplication(t, Config{
		OAuthConfig: utils.OAuthConfig{
			ClientID:     "client-id",
			ClientSecret: "client-secret",
			Endpoint: oauth2.Endpoint{
				AuthURL:   "http://oauth",
				AuthStyle: oauth2.AuthStyleInParams,
			},
			RedirectURL:     "http://app/",
			Scopes:          []string{"calendar"},
			WellKnownPrefix: "",
		},
	})

	userId, err := uuid.NewUUID()
	assert.NoError(t, err)

	resp, err := application.Client.Get("/users/" + userId.String() + "/boundAccounts/google/initiate")
	assert.NoError(t, err)
	assert.Equal(t, 303, resp.StatusCode)

	parsedOAuthURL, err := url.Parse(resp.Header.Get("Location"))
	assert.NoError(t, err)

	assert.Equal(t, "client-id", parsedOAuthURL.Query().Get("client_id"))
	assert.Equal(t, "http://app/", parsedOAuthURL.Query().Get("redirect_uri"))
	assert.Equal(t, "code", parsedOAuthURL.Query().Get("response_type"))
	assert.Equal(t, "calendar", parsedOAuthURL.Query().Get("scope"))

	state := parsedOAuthURL.Query().Get("state")
	token, parts, err := jwt.NewParser().ParseUnverified(state, jwt.MapClaims{})
	assert.NoError(t, err)
	assert.NotNil(t, token)
	assert.NotEmpty(t, parts)

	sub, err := token.Claims.GetSubject()
	assert.NoError(t, err)
	assert.Equal(t, userId.String(), sub)
}

func TestApp_OAuthCallback(t *testing.T) {
	m := mocha.New(t)
	m.Start()
	m.CloseOnCleanup(t)

	m.AddMocks(
		mocha.Post(expect.URLPath("/oauth/token")).
			FormField("client_id", expect.ToEqual("client-id")).
			Reply(
				reply.OK().BodyJSON(
					map[string]string{
						"access_token": "token",
					},
				).Header("Content-Type", "application/json"),
			),
	)
	m.AddMocks(
		mocha.Get(expect.URLPath("/.well-known/openid-configuration")).
			Reply(
				reply.OK().BodyJSON(
					map[string]string{
						"authorization_endpoint": "http://oauth",
						"token_endpoint":         m.URL() + "/oauth/token",
						"userinfo_endpoint":      m.URL() + "/userinfo",
					},
				),
			),
	)
	m.AddMocks(
		mocha.Get(expect.URLPath("/userinfo")).
			Query("access_token", expect.ToBePresent()).
			Reply(
				reply.OK().BodyJSON(
					map[string]string{
						"sub":  "12345",
						"name": "a keen tester",
					},
				),
			),
	)

	application := MakeTestApplication(t, Config{
		OAuthConfig: utils.OAuthConfig{
			ClientID:     "client-id",
			ClientSecret: "client-secret",
			Endpoint: oauth2.Endpoint{
				AuthStyle: oauth2.AuthStyleInParams,
			},
			RedirectURL:     "http://app/",
			Scopes:          []string{"calendar"},
			WellKnownPrefix: m.URL(),
		},
	})

	resp, err := application.Client.Get("/auth/google/callback?code=12345")
	assert.NoError(t, err)
	assert.Equal(t, 301, resp.StatusCode)

	parsedOAuthURL, err := url.Parse(resp.Header.Get("Location"))
	assert.NoError(t, err)

	fragmentParts, err := url.ParseQuery(parsedOAuthURL.Fragment)
	assert.NoError(t, err)

	assert.Equal(t, "SUCCESS", fragmentParts.Get("status"))
	token := fragmentParts.Get("token")
	assert.NotEmpty(t, token)

	decoded, parts, err := jwt.NewParser().ParseUnverified(token, jwt.MapClaims{})
	assert.NoError(t, err)
	assert.NotEmpty(t, parts)

	sub, err := decoded.Claims.GetSubject()
	assert.NoError(t, err)

	userId := uuid.MustParse(sub)

	row := application.Db.QueryRow("SELECT 1 FROM users WHERE id = $1", userId)
	assert.NoError(t, row.Err())

	var target int
	err = row.Scan(&target)
	assert.NoError(t, err)
	assert.Equal(t, 1, target)

	msg, err := application.WorkerConsumer.ReadMessage(context.TODO())
	assert.NoError(t, err)
	cmd, err := command.ParseJSONCommand(msg.Value)
	assert.NoError(t, err)
	assert.Equal(t, &command.LoadCalendarsCommand{GoogleAccountId: "12345"}, cmd)
}
