package app

import (
	"CalendarDeviationCalculator/internal/api/utils"
	"CalendarDeviationCalculator/internal/common/app"
	"CalendarDeviationCalculator/internal/worker/service"
	"context"
	"database/sql"
	"github.com/segmentio/kafka-go"
	"github.com/stretchr/testify/assert"
	"net/http"
	"testing"
)

type TestApplication struct {
	Client         *http.Client
	Db             *sql.DB
	WorkerConsumer *kafka.Reader
}

func cleanupDB(db *sql.DB) {
	tx, _ := db.BeginTx(context.TODO(), nil)
	defer tx.Commit()

	tx.Exec("DELETE FROM google_accounts")
	tx.Exec("DELETE FROM users")
}

func MakeTestApplication(t *testing.T, cfg Config) TestApplication {
	cfg.PostgresConfig = PostgresConfig{ConnectionString: "postgres://postgres:postgres@postgres:5432/postgres?sslmode=disable"}
	cfg.KafkaConnectionConfig = KafkaConnectionConfig{BootstrapServers: []string{"kafka:9092"}}
	cfg.WorkerKafkaConfig = service.WorkerKafkaConfig{Topic: "executions"}
	apiApp, err := MakeApp(cfg, utils.MakeDummyLogger())
	assert.NoError(t, err)

	testApp, err := app.NewTestWebApplication(apiApp)
	assert.NoError(t, err)

	client := app.BootstrapWebApp(t, testApp)

	db, err := sql.Open("postgres", string(cfg.PostgresConfig.ConnectionString))
	assert.NoError(t, err)
	t.Cleanup(func() { cleanupDB(db) })

	/*bareConn, err := net.Dial("tcp", cfg.KafkaConnectionConfig.BootstrapServers[0])
	assert.NoError(t, err)

	bareClient := kafka.NewConnWith(bareConn, kafka.ConnConfig{})
	err = bareClient.CreateTopics(kafka.TopicConfig{
		Topic:             cfg.WorkerKafkaConfig.Topic,
		NumPartitions:     1,
		ReplicationFactor: 1,
	})
	assert.NoError(t, err)*/
	workerConsumer := kafka.NewReader(kafka.ReaderConfig{
		Brokers:        cfg.KafkaConnectionConfig.BootstrapServers,
		GroupID:        "test-12345",
		GroupTopics:    []string{cfg.WorkerKafkaConfig.Topic},
		CommitInterval: 0,
		StartOffset:    kafka.LastOffset,
	})
	//print(workerConsumer.Offset())
	//preloadCtx, _ := context.WithTimeout(context.Background(), time.Millisecond*500)
	//workerConsumer.ReadMessage(preloadCtx)
	//print(workerConsumer.Offset())

	t.Cleanup(func() { workerConsumer.Close() })

	return TestApplication{
		Client:         client,
		Db:             db,
		WorkerConsumer: workerConsumer,
	}
}
