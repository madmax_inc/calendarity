package service

import (
	web_entity "CalendarDeviationCalculator/internal/api/entity"
	"CalendarDeviationCalculator/internal/api/security"
	"CalendarDeviationCalculator/internal/common/entity"
	"context"
	"database/sql"
	"encoding/json"
	"errors"
	"github.com/golang-jwt/jwt/v5"
	"github.com/google/uuid"
	"time"
)

type AuthorizationStateManglerService interface {
	MangleUserId(ctx context.Context, userId entity.UserIdentifier) (string, error)
	DemangleUserId(ctx context.Context, mangledUserId string) (entity.UserIdentifier, error)
}

type JWTManglerService struct {
	authority security.TokenAuthority
	validity  time.Duration
}

func NewJWTManglerService(authority security.TokenAuthority, validity time.Duration) *JWTManglerService {
	return &JWTManglerService{authority: authority, validity: validity}
}

func (m *JWTManglerService) MangleUserId(ctx context.Context, userId entity.UserIdentifier) (string, error) {
	return m.authority.IssueJWT(jwt.RegisteredClaims{Subject: userId.String(), ExpiresAt: jwt.NewNumericDate(time.Now().Add(m.validity))})
}
func (m *JWTManglerService) DemangleUserId(ctx context.Context, mangledUserId string) (entity.UserIdentifier, error) {
	token, err := m.authority.CheckJWT(mangledUserId)
	if err != nil {
		return entity.UserIdentifier{}, err
	}

	sub, err := token.Claims.GetSubject()
	if err != nil {
		return entity.UserIdentifier{}, err
	}

	return uuid.Parse(sub)
}

type ConditionalAuthResult struct {
	UserId    entity.UserIdentifier
	IsCreated bool
}

type AuthorizationService interface {
	UpsertUser(ctx context.Context, userMeta web_entity.UserMeta, googleAccount web_entity.AssociatedGoogleAccount) (ConditionalAuthResult, error)
	BindGoogleAccount(ctx context.Context, userId entity.UserIdentifier, googleAccount web_entity.AssociatedGoogleAccount) (ConditionalAuthResult, error)
}

type SQLRepoAuthorizationService struct {
	db *sql.DB
}

func NewSQLRepoAuthorizationService(db *sql.DB) *SQLRepoAuthorizationService {
	return &SQLRepoAuthorizationService{db: db}
}

func (svc *SQLRepoAuthorizationService) UpsertUser(ctx context.Context, userMeta web_entity.UserMeta, googleAccount web_entity.AssociatedGoogleAccount) (result ConditionalAuthResult, err error) {
	tx, err := svc.db.BeginTx(ctx, nil)
	if err != nil {
		return
	}

	defer func() {
		if err != nil {
			tx.Rollback()
		} else {
			tx.Commit()
		}
	}()

	row := tx.QueryRowContext(
		ctx,
		"UPDATE google_accounts SET display_name = $2, access_token = $3, expires_at = $4, updated_at = NOW()::TIMESTAMP WHERE id = $1 RETURNING user_id",
		googleAccount.AccountId, googleAccount.DisplayName, googleAccount.AccessToken.AccessToken, googleAccount.AccessToken.Expiry,
	)
	err = row.Err()
	if err != nil {
		return
	}

	result.IsCreated = false
	err = row.Scan(&result.UserId)
	if err != nil {
		if !errors.Is(err, sql.ErrNoRows) {
			return
		}
		err = nil
		result.UserId = uuid.New()
		result.IsCreated = true
	}

	userData, err := json.Marshal(userMeta)
	if err != nil {
		return
	}
	_, err = tx.ExecContext(
		ctx,
		"INSERT INTO users(id, user_data) VALUES($1, $2) "+
			"ON CONFLICT (id) DO UPDATE "+
			"SET user_data = $2",
		result.UserId, userData,
	)

	if result.IsCreated {
		_, err = tx.ExecContext(
			ctx,
			"INSERT INTO google_accounts(id, user_id, display_name, access_token, expires_at, created_at, updated_at) "+
				"VALUES($1, $2, $3, $4, $5, NOW()::TIMESTAMP, NOW()::TIMESTAMP)",
			googleAccount.AccountId, result.UserId, googleAccount.DisplayName, googleAccount.AccessToken.AccessToken, googleAccount.AccessToken.Expiry,
		)
	}

	return
}

func (svc *SQLRepoAuthorizationService) BindGoogleAccount(ctx context.Context, userId entity.UserIdentifier, googleAccount web_entity.AssociatedGoogleAccount) (result ConditionalAuthResult, err error) {
	row := svc.db.QueryRowContext(
		ctx,
		"INSERT INTO google_accounts(id, user_id, display_name, acccess_token, expires_at, created_at, updated_at) "+
			"VALUES($1, $2, $3, $4, NOW()::TIMESTAMP, NOW()::TIMESTAMP) "+
			"ON CONFLICT ON google_accounts(id) DO UPDATE "+
			"SET access_token = $3, expires_at = $4, updated_at = NOW()::TIMESTAMP "+
			"RETURNING created_at",
		googleAccount.AccountId, userId, googleAccount.DisplayName, googleAccount.AccessToken,
	)

	if err = row.Err(); err != nil {
		return
	}

	result.UserId = userId

	var createdAt time.Time
	err = row.Scan(&createdAt)
	if err != nil {
		return
	}

	result.IsCreated = time.Now().Sub(createdAt).Seconds() < 1.0

	return
}
