package main

import (
	"context"
	"encoding/json"
	"fmt"
	"golang.org/x/oauth2"
	"log"
	"net/http"
	"os"
)

// Retrieve a token, saves the token, then returns the generated client.
func getClient(config *oauth2.Config) *http.Client {
	// The file token.json stores the user's access and refresh tokens, and is
	// created automatically when the authorization flow completes for the first
	// time.
	tokFile := "token.json"
	tok, err := tokenFromFile(tokFile)
	if err != nil {
		tok = getTokenFromWeb(config)
		saveToken(tokFile, tok)
	}
	return config.Client(context.Background(), tok)
}

// Request a token from the web, then returns the retrieved token.
func getTokenFromWeb(config *oauth2.Config) *oauth2.Token {
	authURL := config.AuthCodeURL("state-token", oauth2.AccessTypeOffline)
	fmt.Printf("Go to the following link in your browser then type the "+
		"authorization code: \n%v\n", authURL)

	var authCode string
	if _, err := fmt.Scan(&authCode); err != nil {
		log.Fatalf("Unable to read authorization code: %v", err)
	}

	tok, err := config.Exchange(context.TODO(), authCode)
	if err != nil {
		log.Fatalf("Unable to retrieve token from web: %v", err)
	}
	return tok
}

// Retrieves a token from a local file.
func tokenFromFile(file string) (*oauth2.Token, error) {
	f, err := os.Open(file)
	if err != nil {
		return nil, err
	}
	defer f.Close()
	tok := &oauth2.Token{}
	err = json.NewDecoder(f).Decode(tok)
	return tok, err
}

// Saves a token to a file path.
func saveToken(path string, token *oauth2.Token) {
	fmt.Printf("Saving credential file to: %s\n", path)
	f, err := os.OpenFile(path, os.O_RDWR|os.O_CREATE|os.O_TRUNC, 0600)
	if err != nil {
		log.Fatalf("Unable to cache oauth token: %v", err)
	}
	defer f.Close()
	json.NewEncoder(f).Encode(token)
}

func main() {
	/*ctx := context.Background()
	b, err := os.ReadFile("credentials.json")
	if err != nil {
		log.Fatalf("Unable to read client secret file: %v", err)
	}

	// If modifying these scopes, delete your previously saved token.json.
	config, err := google.ConfigFromJSON(b, calendar.CalendarReadonlyScope)
		if err != nil {
			log.Fatalf("Unable to parse client secret file to config: %v", err)
		}
		client := getClient(config)

		peopleSvc, err := people.NewService(ctx, option.WithHTTPClient(client))
		if err != nil {
			log.Fatalf("Unable to retrieve Calendar client: %v", err)
		}

		peopleResponse, err := peopleSvc.People.Get("people/me").Do()
		if err != nil {
			log.Fatalf("Unable to retrieve Calendar client: %v", err)
		}

		print(peopleResponse.ResourceName, peopleResponse.Names[0].DisplayName)

		srv, err := calendar.NewService(ctx, option.WithHTTPClient(client))
		if err != nil {
			log.Fatalf("Unable to retrieve Calendar client: %v", err)
		}

		watchId := "foobarbaz-yandex"
		deadline := time.Now().Add(time.Hour).UnixMilli()
		_, err = srv.Events.Watch("primary", &calendar.Channel{
			Address:    "https://functions.yandexcloud.net/d4em6le8j5s8626th9ku",
			Expiration: deadline,
			Id:         watchId,
			Type:       "webhook",
		}).Do()
		if err != nil {
			log.Fatalf("error %v", err)
		}


			//beginTime := time.Now().Format(time.RFC3339)
			//endTime := time.Now().Add(time.Hour * 24 * 7).Format(time.RFC3339)
			syncToken := "CPD0jfCpyP8CEPD0jfCpyP8CGAUg9ZiggQI="

			calendars, err := srv.CalendarList.List().Do()
			if err != nil {
				log.Fatalf("Unable to retrieve user's calendars: %v", err)
			}

			for _, calendarObj := range calendars.Items {
				fmt.Printf(calendarObj.Summary)
			}TimeMin(beginTime).TimeMax(endTime)

			events, err := srv.Events.List("primary").ShowDeleted(true).
				SyncToken(syncToken).Do()
			if err != nil {
				log.Fatalf("Unable to retrieve next ten of the user's events: %v", err)
			}
			fmt.Println("Upcoming events:")
			if len(events.Items) == 0 {
				fmt.Println("No upcoming events found.")
			} else {
				for _, item := range events.Items {
					date := item.Start.DateTime
					if date == "" {
						date = item.Start.Date
					}
					fmt.Printf("%v : %v (%v) - %v\n", item.Updated, item.Summary, date, item.Status)
				}
			}

			fmt.Println(events.NextPageToken)
			fmt.Println(events.NextSyncToken)*/
}
