package main

import (
	"CalendarDeviationCalculator/internal/api/app"
	web_app "CalendarDeviationCalculator/internal/common/app"
	"context"
	"flag"
	"fmt"
	"os"
	"os/signal"

	"github.com/sirupsen/logrus"
)

func main() {
	configFile := flag.String("config", "", "a config file to load")
	bindPort := flag.Int("port", 8888, "a port to bind")
	verbose := flag.Bool("verbose", false, "make logging verbose")
	flag.Parse()

	if configFile == nil {
		flag.Usage()
		os.Exit(1)
	}

	logger := logrus.New()
	logger.SetFormatter(&logrus.JSONFormatter{})
	logger.SetOutput(os.Stdout)
	logLevel := logrus.InfoLevel
	if *verbose {
		logLevel = logrus.DebugLevel
	}
	logger.SetLevel(logLevel)

	config, err := app.LoadConfig(*configFile)
	if err != nil {
		logger.Fatal(err)
	}

	application, err := app.MakeApp(config, logger)
	if err != nil {
		logger.Fatal(err)
	}

	ctx, cancel := signal.NotifyContext(context.Background(), os.Interrupt)
	defer cancel()
	if err := web_app.NewWebApplication(fmt.Sprint(":", *bindPort), application).Run(ctx, nil); err != nil {
		logger.Fatal(err)
	}
}
