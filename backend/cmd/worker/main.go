package main

import (
	"CalendarDeviationCalculator/internal/worker/app"
	"context"
	"flag"
	"os"
	"os/signal"

	"github.com/sirupsen/logrus"
)

func main() {
	configFile := flag.String("config", "", "a config file to load")
	verbose := flag.Bool("verbose", false, "make logging verbose")
	flag.Parse()

	if configFile == nil {
		flag.Usage()
		os.Exit(1)
	}

	logger := logrus.New()
	logger.SetFormatter(&logrus.JSONFormatter{})
	logger.SetOutput(os.Stdout)
	logLevel := logrus.InfoLevel
	if *verbose {
		logLevel = logrus.DebugLevel
	}
	logger.SetLevel(logLevel)

	config, err := app.LoadConfig(*configFile)
	if err != nil {
		logger.Fatal(err)
	}

	application, err := app.MakeApp(config, logger)
	if err != nil {
		logger.Fatal(err)
	}

	ctx, cancel := signal.NotifyContext(context.Background(), os.Interrupt)
	defer cancel()
	if err := application.Run(ctx); err != nil {
		logger.Fatal(err)
	}
}
